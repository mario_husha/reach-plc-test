# README #

### Introduction ###

This code provides a framework of an express app to accept, store and display palindromes to solve the palindrome exercise
- https://docs.google.com/document/d/1HpFV-yiSGdxvAK9eOcz0UkeEslqYh9sv_2z6IR-0YIU/edit?usp=sharing.

### Submission ###

Please create your own branch for your changes, and then submit this to the repository once you are done

Note that:

 * Uses mocha, chai, sinon for testing - you should feel free to swap in whatever framework(s) you prefer
 * You can run the application via: 
 ```bash
npm install && npm start
```
 * The palindrome checker needs implementing 
 * The data persistence layer needs implementing
 * Feel free to modify the UI as you wish 
 * There is a basic API defined, but feel free to extend/change this in any way 
 * There is no linting on the project ... should there be?
