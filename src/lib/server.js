
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const port = parseInt(process.env.PORT || '1337', 10);

const palindrome = require('./palindrome');
const db = require('./db');

const checkAndSaveIfValid = (input) => {
  const status = palindrome(input);
  if (status) {
    db.put(input)
  }
  return status;
};

const limit = 10;
const within = 1000 * 60 * 10;
const errors = {
  missing: 'Please submit a palindrome to check!',
  missingVerbose: 'The POST parameter `query` must be non-empty'
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '../view'));

app.use('/static', express.static(path.join(__dirname, '../public')));

app.get('/', (req, res) => {
  res.render('index', {
    minutes: within / (60 * 1000),
    palindromes: db.get(limit, within)
  });
});

app.post('/api/v1/add-palindrome', (req, res) => {
  if (!req.body || !req.body.query) {
    res.status(400).json({
      error: errors.missing,
      errorDetail: errors.missingVerbose
    });
  } else {
    res.status(200).json({
      query: req.body.query,
      palindrome: checkAndSaveIfValid(req.body.query)
    });
  }
});

app.get('/api/v1/list-palindromes', (req, res) => {
  res.status(200).json({
    palindromes: db.get(limit, within)
  });
});

const server = app.listen(port, () => {
  console.log(`Palindrome app started, listening on port ${port}`);
});

module.exports = server;
