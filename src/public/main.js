const htmlEscape = text =>
  text.replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');

const bindPalindromeTester = (form, input, output, callback) => {
  const label = form.querySelector('label[for="' + input.getAttribute('id') + '"]');

  const ifEmpty = (value, fn) => {
    if (!value || !value.replace(/\s/g, '').length) {
      fn();
    }
  };
  const insertMessage = function (html) {
    output.innerHTML = html;
  };

  input.addEventListener('focus', function () {
    label.classList.add('active');
  }, false);
  input.addEventListener('blur',
    () => ifEmpty(this.value, () => label.classList.remove('active')),
    false);
  form.addEventListener('submit',
    e => {
      e.preventDefault();
      fetch('/api/v1/add-palindrome', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: input.value ? JSON.stringify({query: input.value}) : null
      })
      .then(response => response.json())
      .then(json => {
        if (json.error) {
          insertMessage('<p class="error">' + htmlEscape(json.error) + '</p>');
        } else {
          insertMessage('<p class="success">' + htmlEscape(json.query) + ' is' + (json.palindrome ? '' : ' not') + ' a palindrome!</p>');
        }
        input.value = '';
        callback();
      });
    },
    false);
};

const bindLatestPalindromesUpdater = latest => {
  const button = document.createElement('button');
  button.appendChild(document.createTextNode('Refresh list'));
  latest.parentNode.appendChild(button);

  const doUpdate = () => {
    let html = '';

    fetch('/api/v1/list-palindromes')
    .then((response) => {
      return response.json();
    })
    .then(json => {
      if (json.palindromes && json.palindromes.length) {
        html = '<ol>';
        html += json.palindromes.map(p => `<li>${htmlEscape(p)}</li>`).join('');
        html += '</ol>';
      } else {
        html = '<p>No palindromes found</p>'
      }
      latest.innerHTML = html;
    });
  };

  button.addEventListener('click', doUpdate, false);

  return doUpdate;
};

bindPalindromeTester(
  document.getElementById('palindrome-form'),
  document.getElementById('query'),
  document.getElementById('feedback'),
  bindLatestPalindromesUpdater(document.getElementById('latest'))
);
